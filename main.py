# -*- coding: utf-8 -*-

# !/usr/bin/python
# 22.10.15

import json, os, zipfile, glob
import sys, commands

CONST_SETTINGS_FILE = 'config.json'


def loadSettings():
    with open(CONST_SETTINGS_FILE) as  dataFile:
        data = json.load(dataFile)
    return data


def main():
    howToUse = "Programme should be used the following way:  python main.py numberOfWindowToSwitch," \
               "e.g.  'python main.py  1'  will switch to the windows having the 1 number in config.json file "
    if len(sys.argv) != 2:
        print howToUse
        return


    selectedOption = sys.argv[1]

    settings = loadSettings()

    lengthSettings = len(settings['windows'])
    if int(selectedOption) > int(lengthSettings):
        print "Out of range, there are only " + len(settings['windows']) +  " options to select from"
        return

    command = "wmctrl -a " + settings['windows'][sys.argv[1]]

    result = commands.getstatusoutput(command)
    print result

    return True

if __name__ == "__main__":
    main()
