Usage example: 

Run this program as  
  python main.py 1  to switch to Chrome application. 
  
 Note: 
  Chrome should be launched. Otherwise you won't get switched to it.
  
  To see all list of open windows use: 
  <b> wmctrl -l </b>
    


For better convenience it's recommended to use it with<b> xbindkeys</b>.
 
 This is example  of my config for it: 

  "python /home/me/PycharmProjects/windowswitch/main.py 1"
    m:0x50 + c:10
    #Mod2+Mod4 + 1

   "python /home/me/PycharmProjects/windowswitch/main.py 2"
    m:0x50 + c:11
   # Control+Mod2 + 1

  "python /home/me/PycharmProjects/windowswitch/main.py 3"
    m:0x50 + c:12
   # Control+Mod2 + 1
   
  
  As result we can quickly switch to the necessary window without necessety to select. 
